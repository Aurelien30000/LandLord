package biz.princeps.landlord.commands;

public enum ManageMode {

    ONE,
    MULTI,
    ALL

}
