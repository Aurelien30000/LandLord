package biz.princeps.landlord.commands.friends;

import biz.princeps.landlord.api.ILandLord;
import biz.princeps.landlord.api.IOwnedLand;
import biz.princeps.landlord.api.events.LandManageEvent;
import biz.princeps.landlord.commands.LandlordCommand;
import biz.princeps.lib.command.Arguments;
import biz.princeps.lib.command.Properties;
import biz.princeps.lib.exception.ArgumentsOutOfBoundsException;
import com.google.common.collect.Sets;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Project: LandLord
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 17/7/17
 */
public class UnfriendAll extends LandlordCommand {

    public UnfriendAll(ILandLord pl) {
        super(pl, pl.getConfig().getString("CommandSettings.RemovefriendAll.name"),
                pl.getConfig().getString("CommandSettings.RemovefriendAll.usage"),
                Sets.newHashSet(pl.getConfig().getStringList("CommandSettings.RemovefriendAll.permissions")),
                Sets.newHashSet(pl.getConfig().getStringList("CommandSettings.RemovefriendAll.aliases")));
    }

    @Override
    public void onCommand(Properties properties, Arguments arguments) {
        if (properties.isConsole()) {
            return;
        }
        try {
            onUnfriendall(properties.getPlayer(), arguments.get(0));
        } catch (ArgumentsOutOfBoundsException e) {
            onUnfriendall(properties.getPlayer(), null);
        }
    }

    private void onUnfriendall(Player player, String name) {
        if (isDisabledWorld(player)) {
            return;
        }

        if (name == null || name.isEmpty()) {
            lm.sendMessage(player, lm.getString(player, "Commands.Addfriend.noPlayer")
                    .replace("%players%", "?"));
            return;
        }

        plugin.getPlayerManager().getOffline(name, (offline) -> {
            if (offline == null) {
                // Failure
                lm.sendMessage(player, lm.getString(player, "Commands.UnfriendAll.noPlayer")
                        .replace("%players%", name));
            } else {
                // Success
                Bukkit.getScheduler().runTaskAsynchronously(plugin.getPlugin(), () -> {
                    int count = 0;
                    for (IOwnedLand ol : plugin.getWGManager().getRegions(player.getUniqueId())) {
                        if (ol.isFriend(offline.getUuid())) {
                            String oldvalue = ol.getMembersString();
                            ol.removeFriend(offline.getUuid());
                            count++;
                            Bukkit.getScheduler().runTask(plugin.getPlugin(), () -> {
                                LandManageEvent landManageEvent = new LandManageEvent(player, ol,
                                        "FRIENDS", oldvalue, ol.getMembersString());
                                Bukkit.getPluginManager().callEvent(landManageEvent);
                            });
                        }
                    }

                    if (count > 0) {
                        lm.sendMessage(player, lm.getString(player, "Commands.UnfriendAll.success")
                                .replace("%count%", String.valueOf(count))
                                .replace("%players%", name));

                        Bukkit.getScheduler().runTask(plugin.getPlugin(), plugin.getMapManager()::updateAll);
                    } else {
                        lm.sendMessage(player, lm.getString(player, "Commands.UnfriendAll.noFriend")
                                .replace("%player%", name));
                    }
                });
            }
        });
    }
}

